#pragma once

#include "card.h"

namespace Eval {

	// returns poker_eval score for a given poker hand
	int HandValue(Card* cards, int nrCards);


	// quick and dirty function that returns SCOOP score for a 7 cards hand score (returned by Eval_HandValue)
	int ScoopScore(int v);

}