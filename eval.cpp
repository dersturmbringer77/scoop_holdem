#include "eval.h"
#include "poker-eval/include/poker_defs.h"
#include "poker-eval/include/inlines/eval.h"

namespace Eval {

	// function used internally by Eval_HandValue 
	// we could later replace it with a lookup table for speed since we have only 54 possible cards
	static uint64 ConvertToCardMask(Card& c0){
		CardMask c;
		c.cards_n = 0;
		switch(c0.suite()){
		case 0: c.cards.hearts = (1 << (c0.value()-2)); break;
		case 1: c.cards.diamonds = (1 << (c0.value()-2)); break;
		case 2: c.cards.clubs = (1 << (c0.value()-2)); break;
		case 3: c.cards.spades = (1 << (c0.value()-2)); break;
		}
		return c.cards_n;
	}


	// returns poker_eval score for a given poker hand
	int HandValue(Card* cards, int nrCards)
	{
		int i;
		CardMask c;
		c.cards_n = 0;
		for (i = 0; i < nrCards; i++) c.cards_n |= ConvertToCardMask(cards[i]);
		return Hand_EVAL_N(c, nrCards);
	}


	// quick and dirty function that returns SCOOP score for a 7 cards hand score (returned by Eval_HandValue)
	int ScoopScore(int v)
	{
		#define _check(X,VAL) if ((v & X)==X) return VAL;

		v >>= 24;
		_check(StdRules_HandType_STFLUSH, 20);
		_check(StdRules_HandType_QUADS, 10);
		_check(StdRules_HandType_FULLHOUSE, 6);
		_check(StdRules_HandType_FLUSH, 5);
		_check(StdRules_HandType_STRAIGHT, 4);
		_check(StdRules_HandType_TRIPS, 3);
		_check(StdRules_HandType_TWOPAIR, 2);
		return 1;
	}

}

