#pragma once

#include <stdexcept>
#include <iostream>

class Card;

namespace Cards {
	namespace Constants {
		// card values will be integers 2..15, assuming we include jokers in the mix
		const int T=10;
		const int J=11;
		const int Q=12;
		const int K=13;
		const int A=14;
		const int JOKER=15;

		// card suites
		const int H=0;
		const int D=1;
		const int C=2;
		const int S=3;
	}

	using Pair = std::pair<Card,Card>;
}

// representation of a playing card
// if we assume we have a deck of playing cards with 2 jokers
// then each card can be encoded as a number between 0..53
class Card {
public:
	// by default a card will be 2H
	Card() : m_idx(0) {}
		
	// init a card by index 0..53
	Card(unsigned char idx);

	// inits card from the first 2 characters in a string
	Card(const std::string& s);

	// init a card by value 2..15 (JOKER) and suite 0..3 (Heart/Diamond/Club/Spade)
	Card(unsigned char value, unsigned char suite);

	unsigned char idx() const { return m_idx; }
	unsigned char value() const { return (m_idx>>2)+2; }
	unsigned char suite() const { return m_idx&3; }

	// returns a string representation of the card
	std::string to_str();

protected:
	unsigned char m_idx;	
};
