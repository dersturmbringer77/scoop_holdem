#pragma once

#include <set>
#include <span>
#include <array>
#include <vector>
#include <stdexcept>

#include "card.h"
#include "cache.h"

namespace Scoop {

	struct Hand {	
		Hand(){}
		Hand(std::span<Card> src);

		std::string to_str();

		std::array<Card,7> cards;
		double score;
		int n_score;
	};

	double score_hero_vs_opp(std::span<Card> hero, std::span<Card> opp, std::span<Card> flop, std::span<Cards::Pair> turn_river_cards);
	double score_hero_vs_opp_w_cache(std::span<Card> hero, std::span<Card> opp, std::span<Card> flop, std::span<Cards::Pair> turn_river_cards, std::array<char,1024> &invalid_turn_river_idx_p1, std::array<char,1024> &invalid_turn_river_idx_p2);

	std::vector<Hand> gen_all_possible_hands(std::array<Card,7> cards);

	// given 7 cards for hero and 3 cards for flop: generates a vector of all distinct card pairs for possible turn+rivers
	// turn_card.idx() < river_card.idx() always
	std::vector<Cards::Pair> gen_all_turn_river_cards(std::span<Card> cards, std::array<Card,3> flop);

	// returns a list of indices in turn_river_cards that contain any card from cards_to_exclude
	std::array<char,1024> gen_all_invalid_turn_rivers_indices(std::span<Cards::Pair> turn_river_cards, std::span<Card> cards_to_exclude);
}

