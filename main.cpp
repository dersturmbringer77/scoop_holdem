#include <array>
#include <vector>
#include <ranges>
#include <iostream>
#include <algorithm>
#include <exception>
#include <numeric>
#include <chrono>
#include <random>

#include <thread>

#include "card.h"
#include "eval.h"
#include "scoop_hand.h"

using std::cout, std::endl;

using ScoopHand = Scoop::Hand;

const int N_PROC = 5;	// number of threads to use

// we'll use this structure to keep a vector of players
struct PlayerInfo {
	std::array<Card,7> cards;						// cards, just in case, probably not going to need them
	std::vector<ScoopHand> all_possible_hands;		// ordered list of possible hands, best solution will be always first
	std::uint64_t card_mask;						// a mask for the 7 cards dealt to this player
	std::array<char,1024> invalid_turn_river_idx;	// indices in the turn_river list for entries that are invalid for this player
	size_t id;
};


struct HandInfo {
	std::span<Card> flop;
	std::span<Cards::Pair> turn_river;

	HandInfo(std::span<Card> _flop, std::span<Cards::Pair> _turn_river) 
		: flop(_flop), turn_river(_turn_river) {}
};


// helper function, returns a 64 bit mask with indices set for given cards in a span
inline std::uint64_t gen_card_mask(std::span<Card> cards)
{
	std::uint64_t mask=0;
	for (auto& c: cards) mask |= (1ll << c.idx());
	return mask;
}




// score all possible hands given against maximum 20 turn+river, keep top MAX_KEEP
void keep_top_hands(std::vector<ScoopHand>& hands, std::vector<Cards::Pair>& turn_river, const int MAX_KEEP=30)
{
	size_t i, c1_idx, c2_idx;

	std::set<size_t> all_turn_rivers;
	std::set<size_t> _50_turn_rivers;
	auto gen = std::mt19937{std::random_device{}()};

	for (i = 0; i < turn_river.size(); i++) all_turn_rivers.insert(i);
	std::sample(all_turn_rivers.begin(), all_turn_rivers.end(), std::inserter(_50_turn_rivers, _50_turn_rivers.begin()), 50, gen);

	for (auto& h:hands){
		auto& hero = h.cards;

		float score = 0;
		int n = 0;
		for (size_t turn_river_idx=0; turn_river_idx < turn_river.size(); turn_river_idx++){

			if (!_50_turn_rivers.contains(turn_river_idx)) continue;

			for (i = 0; i < 3; i++){
				c1_idx = hero[i*2].idx();
				c2_idx = hero[i*2+1].idx();
				int eval_hero = Scoop::master_cache.get_hand_value(c1_idx, c2_idx, turn_river_idx);

				score += Eval::ScoopScore(eval_hero);
			}
			n += 1;

			if (n >= 20) break;	// we take just 20 valid turn/rivers, want to get some quick results
		}

		h.score = score*1.0f/n;
	}

	std::sort(hands.begin(), hands.end(), [](const ScoopHand& left, const ScoopHand& right) { return left.score > right.score; });

	hands.resize(MAX_KEEP);
}



struct CachePayload {
	std::vector<PlayerInfo> players;
	std::vector<Scoop::Cache> cache;
	std::span<Cards::Pair> turn_river;
};


void fill_cache_func(CachePayload& payload)
{
	size_t i,j,turn_river_idx;
	for (i = 0; i < payload.players.size(); i++){
		auto& p = payload.players[i];
		auto& cache = payload.cache[i];
		for (auto& h: p.all_possible_hands){
			for (j = 0; j < 3; j++){
				size_t c1_idx = h.cards[j*2+0].idx();
				size_t c2_idx = h.cards[j*2+1].idx();
				for (turn_river_idx = 0; turn_river_idx < payload.turn_river.size(); turn_river_idx++)
					cache.compute_hand_value(c1_idx, c2_idx, turn_river_idx);
			}
		}
	}
}


// for opponents and hero fills a cache with all possible hand values for all values for turn/river
// p0 is our hero
// players is the list of opponents
// it will assign to each thread a number of players and evaluate all hands
// when all is done will concatenate all computed hand values to a master cache
void fill_master_cache(PlayerInfo& p0, std::vector<PlayerInfo>& players, HandInfo& hand_info)
{
	size_t i, current_thread;
	std::array<std::jthread, N_PROC> threads;
	std::array<CachePayload, N_PROC> payload;

	Scoop::master_cache.init(hand_info.flop, hand_info.turn_river);

	// prepare threads
	for (i = 0; i < N_PROC; i++)
		payload[i].turn_river = hand_info.turn_river;

	current_thread=0;
	payload[current_thread].players.push_back(p0);
	payload[current_thread].cache.push_back(Scoop::master_cache);		
	current_thread = (current_thread+1)%N_PROC;
	for (i = 0; i < players.size(); i++, current_thread = (current_thread+1)%N_PROC){
		payload[current_thread].players.push_back(players[i]);
		payload[current_thread].cache.push_back(Scoop::master_cache);		
	}

	// runs them
	for (i = 0; i < N_PROC; i++)
		threads[i] = std::jthread([&](size_t id) { fill_cache_func(payload[id]); }, i);

	for (i = 0; i < N_PROC; i++)
		threads[i].join();

	// concatenate results
	for (i = 0; i < N_PROC; i++)
		for (auto& cache: payload[i].cache)
			Scoop::master_cache += cache;
}




struct PlayerPassPayload {
	float threshold;
	bool just_top_solution;
	std::span<Card> flop;
	std::vector<PlayerInfo> players;
	std::span<Cards::Pair> turn_river;
};


std::vector<double> softmax_player(PlayerInfo& p){
	std::vector<double> ret;
	for (auto& x: p.all_possible_hands) 
		ret.push_back(x.score);

	double minn = *(std::min_element(ret.begin(), ret.end()));
	double maxx = *(std::max_element(ret.begin(), ret.end()));

	if (minn > 0) minn = 0;

	for (auto& v: ret)
		v = (v-minn)/(maxx-minn);

	return ret;
};


std::vector<ScoopHand> get_top_hands(PlayerInfo& p, double threshold){
	if (threshold <= 0 || threshold > 1) throw std::out_of_range("get_top_hands: bad threshold");
	auto prob = softmax_player(p);
	std::vector<ScoopHand> ret;
	for (size_t i=0; auto& hand: p.all_possible_hands)
		if (prob[i++] >= threshold)
			ret.push_back(hand);
	return ret;
};



void player_pass_func(PlayerPassPayload& payload)
{
	auto new_players(payload.players);
	for (auto& p: new_players) p.all_possible_hands = get_top_hands(p, payload.threshold);

	for (auto& p1: payload.players){
		for (auto& h1: p1.all_possible_hands){
			h1.score = 0;
			h1.n_score = 0;

			for (auto& p2: new_players){
				if (p2.card_mask & p1.card_mask) continue;
				double score = 0;
				int n = 0;
				for (auto& h2: p2.all_possible_hands){
					score += (double)Scoop::score_hero_vs_opp_w_cache(h1.cards, h2.cards, payload.flop, payload.turn_river, p1.invalid_turn_river_idx, p2.invalid_turn_river_idx);
					n += 1;

					if (payload.just_top_solution) break;
				}
				h1.score += score/n;
				h1.n_score += 1;
			}
			h1.score /= h1.n_score;
		}
	}
	for (auto& p1: payload.players)
		std::sort(p1.all_possible_hands.begin(), p1.all_possible_hands.end(), [](const ScoopHand& left, const ScoopHand& right) { return left.score > right.score; });
}



std::vector<PlayerInfo> do_pass_multithreaded(std::vector<PlayerInfo>& players, HandInfo& hand_info, float threshold)
{
	size_t i, current_thread;
	std::array<std::jthread, N_PROC> threads;
	std::array<PlayerPassPayload, N_PROC> payload;

	// prepare threads
	for (i = 0, current_thread=0; i < players.size(); i++, current_thread = (current_thread+1)%N_PROC)
		payload[current_thread].players.push_back(players[i]);
	for (i = 0; i < 5; i++){
		payload[i].flop = hand_info.flop;
		payload[i].threshold = threshold;
		payload[i].turn_river = hand_info.turn_river;
		payload[i].just_top_solution = threshold >= 0.99;
	}

	// runs them
	for (i = 0; i < N_PROC; i++)
		threads[i] = std::jthread([&](size_t id) { player_pass_func(payload[id]); }, i);

	for (i = 0; i < N_PROC; i++)
		threads[i].join();

	// return new player list
	std::vector<PlayerInfo> new_players;
	for (i = 0; i < N_PROC; i++)
		for (auto& p: payload[i].players)
			new_players.push_back(p);
	return new_players;
}



Scoop::Hand compute_best_hand_vs_one_opp(PlayerInfo& p0, std::vector<PlayerInfo>& opponents, std::span<Card> flop, std::span<Cards::Pair> turn_river, float threshold)
{
	for (auto& h0: p0.all_possible_hands){
		h0.score = 0;
		h0.n_score = 0;

		for (auto& p1: opponents){
			auto& h1 = p1.all_possible_hands[0];
			double score = Scoop::score_hero_vs_opp_w_cache(h0.cards, h1.cards, flop, turn_river, p0.invalid_turn_river_idx, p1.invalid_turn_river_idx);
			h0.score += score;
			h0.n_score += 1;
		}
		h0.score /= h0.n_score;
	}

	std::sort(p0.all_possible_hands.begin(), p0.all_possible_hands.end(), [](const ScoopHand& left, const ScoopHand& right) { return left.score > right.score; });
	return p0.all_possible_hands[0];
}



Scoop::Hand compute_best_hand_vs_two_opp(PlayerInfo& p0, std::vector<PlayerInfo>& opponents, std::span<Card> flop, std::span<Cards::Pair> turn_river, float threshold)
{
	auto gen = std::mt19937{std::random_device{}()};
	std::uniform_int_distribution<size_t> uni(0,opponents.size()-1);

	const int N_PAIRS = 50;
	for (size_t ii = 0; ii < N_PAIRS; ii++){
		PlayerInfo p1,p2;

		// choose two opponents that have no cards in common between them
		do {
			p1 = opponents[uni(gen)];
			p2 = opponents[uni(gen)];
		} while ((p1.card_mask & p2.card_mask)!=0);

		for (auto& h0: p0.all_possible_hands){
			// score1 = p0 vs p1 top hand
			auto& h1 = p1.all_possible_hands[0];
			double score1 = Scoop::score_hero_vs_opp_w_cache(h0.cards, h1.cards, flop, turn_river, p0.invalid_turn_river_idx, p1.invalid_turn_river_idx);

			// score1 = p0 vs p2
			auto& h2 = p2.all_possible_hands[0];
			double score2 = Scoop::score_hero_vs_opp_w_cache(h0.cards, h2.cards, flop, turn_river, p0.invalid_turn_river_idx, p2.invalid_turn_river_idx);

			h0.score += score1+score2;
			h0.n_score += 1;
		}
	}

	for (auto& h0: p0.all_possible_hands)
		h0.score /= h0.n_score;

	std::sort(p0.all_possible_hands.begin(), p0.all_possible_hands.end(), [](const ScoopHand& left, const ScoopHand& right) { return left.score > right.score; });
	return p0.all_possible_hands[0];
}











void solve_hero_vs_many_opp(std::array<Card,7> hero_cards, std::array<Card,3> flop, const int N_OPP=50)
{
	size_t i;

	auto turn_river = Scoop::gen_all_turn_river_cards(hero_cards, flop);
	auto all_hero_possible_hero_hands = Scoop::gen_all_possible_hands(hero_cards);

	HandInfo hand_info(flop, turn_river);

	// vector of N_OPP+1 players, first one is our hero
	PlayerInfo p0;
	p0.cards = hero_cards;
	p0.all_possible_hands = Scoop::gen_all_possible_hands(hero_cards);
	p0.card_mask = gen_card_mask(hero_cards);
	std::fill(p0.invalid_turn_river_idx.begin(), p0.invalid_turn_river_idx.end(), 0);
	p0.id = 0;

	// determine remaining cards in deck
	std::int64_t seen_cards=0;
	for (auto& c: flop) seen_cards |= (1ll << c.idx());
	for (auto& c: hero_cards) seen_cards |= (1ll << c.idx());

	std::vector<Card> cards_in_deck;
	for (i = 0; i < 52; i++)
		if (((seen_cards>>i)&1) == 0)
			cards_in_deck.push_back(Card((unsigned char)i));

	// generate random N_OPP opponents
	std::vector<PlayerInfo> players;
	auto gen = std::mt19937{std::random_device{}()};
	for (i = 0; i < N_OPP; i++){
		PlayerInfo p;

		std::vector<Card> cards_sample;
		std::sample(cards_in_deck.begin(), cards_in_deck.end(), std::back_inserter(cards_sample), 7, gen);
		std::copy(cards_sample.begin(), cards_sample.end(), p.cards.begin());

		p.all_possible_hands = Scoop::gen_all_possible_hands(p.cards);
		p.card_mask = gen_card_mask(p.cards);
		p.invalid_turn_river_idx = Scoop::gen_all_invalid_turn_rivers_indices(turn_river, p.cards);
		p.id = players.size();
		players.push_back(p);
	}
	
	fill_master_cache(p0, players, hand_info);

	// for each player keep top hands...
	keep_top_hands(p0.all_possible_hands, turn_river);
	for (auto& p: players)
		keep_top_hands(p.all_possible_hands, turn_river);


	// at this point we should have exactly N_OPP+1 players
	if (players.size() != N_OPP) throw std::runtime_error("invalid number of players");

	players = do_pass_multithreaded(players, hand_info, 1.0f);	// first pass vs naive hands
	players = do_pass_multithreaded(players, hand_info, 0.85f); // 2nd pass vs hands from 1st pass

	auto best_hand = compute_best_hand_vs_one_opp(p0, players, flop, turn_river, 0.99f);
	auto best_hand2 = compute_best_hand_vs_two_opp(p0, players, flop, turn_river, 0.99f);

	cout << "[pass2]\n\n";
	cout << "[vs one player]:  "<< best_hand.to_str() << " -> " << best_hand.score << endl;
	cout << "[vs two players]: "<< best_hand2.to_str() << " -> " << best_hand2.score << endl;

	cout << "\n[pass3]\n\n";

	players = do_pass_multithreaded(players, hand_info, 0.85f); // 3rd pass: vs 2nd pass hands

	best_hand = compute_best_hand_vs_one_opp(p0, players, flop, turn_river, 0.99f);
	best_hand2 = compute_best_hand_vs_two_opp(p0, players, flop, turn_river, 0.99f);

	cout << "[vs one player]:  "<< best_hand.to_str() << " -> " << best_hand.score << endl;
	cout << "[vs two players]: "<< best_hand2.to_str() << " -> " << best_hand2.score << endl;

	cout << endl;
}







int main(int argc, char* argv[])
{
	size_t i;
	
	using namespace Cards::Constants;
	std::array<Card,7> hero;
	std::array<Card,7> opp;
	std::array<Card,3> flop;
	
	auto start = std::chrono::high_resolution_clock::now();

	if (argc >= 3){
		char* s1 = argv[1];
		char* s2 = argv[2];

		cout << s1 << " " << s2 << endl;

		if (strlen(s1) < 14) throw std::length_error("s1 too small");
		if (strlen(s2) < 6) throw std::length_error("s2 too small");

		for (i = 0; i < 7; i++) hero[i] = Card(s1+i*2);
		for (i = 0; i < 3; i++) flop[i] = Card(s2+i*2);

		solve_hero_vs_many_opp(hero, flop, 50);
	} else {
		cout << "Usage:\n";
		cout << "  " << argv[0] << "[hero hand 7 cards] [flop 3 cards]\n";
	}

	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop-start);
	cout << "total time: " << duration.count()/1000000.0f << endl;

	return 0;
}

