#pragma once

#include <span>
#include <cstdint>

#include "card.h"

namespace Scoop {

	struct Cache {
		Cache();
		
		void init(std::span<Card> _flop, std::span<Cards::Pair> _turn_river_cards);
		int compute_hand_value(size_t c1_idx, size_t c2_idx, size_t turn_river_idx);
		int get_hand_value(size_t c1_idx, size_t c2_idx, size_t turn_river_idx) const;
		Cache& operator+=(const Cache& rhs);

		bool initialized() const;
		
	protected:
		bool is_initialized;
		Card board[7];
		std::span<Card> flop;
		std::span<Cards::Pair> turn_river_cards;
		std::int32_t cache[52*52*861];
	};

	extern Cache master_cache;

}