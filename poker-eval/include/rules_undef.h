/*
 * Copyright (C) 2004 
 *           Brian Goetz <brian@quiotix.com>
 *           Loic Dachary <loic@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#undef HandType_NOPAIR    
#undef HandType_ONEPAIR   
#undef HandType_TWOPAIR   
#undef HandType_TRIPS     
#undef HandType_STRAIGHT  
#undef HandType_FLUSH     
#undef HandType_FULLHOUSE 
#undef HandType_QUADS     
#undef HandType_STFLUSH   
#undef HandType_FIRST     
#undef HandType_COUNT     
#undef HandType_LAST      

#undef handTypeNames        
#undef handTypeNamesPadded  
#undef nSigCards            
#undef HandVal_print        
#undef HandVal_toString     
