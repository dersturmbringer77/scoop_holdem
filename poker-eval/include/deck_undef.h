/*
 * Copyright (C) 2004 
 *           Brian Goetz <brian@quiotix.com>
 *           Loic Dachary <loic@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#undef Deck_N_CARDS      
#undef Deck_MASK         
#undef Deck_RANK         
#undef Deck_SUIT         

#undef Rank_2            
#undef Rank_3            
#undef Rank_4            
#undef Rank_5            
#undef Rank_6            
#undef Rank_7            
#undef Rank_8            
#undef Rank_9            
#undef Rank_TEN          
#undef Rank_JACK         
#undef Rank_QUEEN        
#undef Rank_KING         
#undef Rank_ACE          
#undef Rank_FIRST        
#undef Rank_COUNT        

#undef Suit_HEARTS       
#undef Suit_DIAMONDS     
#undef Suit_CLUBS        
#undef Suit_SPADES       
#undef Suit_FIRST        
#undef Suit_COUNT        

#undef CardMask               
#undef CardMask_OR            
#undef CardMask_SET           
#undef CardMask_CARD_IS_SET   
#undef CardMask_ANY_SET       
#undef CardMask_RESET         

#undef CardMask_SPADES        
#undef CardMask_HEARTS        
#undef CardMask_CLUBS         
#undef CardMask_DIAMONDS      
#undef CardMask_SET_SPADES        
#undef CardMask_SET_HEARTS        
#undef CardMask_SET_CLUBS         
#undef CardMask_SET_DIAMONDS      

#undef Deck_cardToString
#undef Deck_maskToString
#undef Deck_printCard
#undef Deck_printMask

#undef CurDeck
