#include "cache.h"
#include "eval.h"

namespace Scoop 
{
	#define EXTRA_DEBUG_CHECKS 0

	Cache master_cache;

	Cache::Cache()
	{
		is_initialized = false;
	}
		
	bool Cache::initialized() const
	{
		return is_initialized;	
	}	


	void Cache::init(std::span<Card> _flop, std::span<Cards::Pair> _turn_river_cards)
	{
			if (_flop.size() != 3) throw std::length_error("flop must have 3 cards");
			if (_turn_river_cards.size() < 1) throw std::length_error("turn_river_cards must have at least size 1");
			if (turn_river_cards.size() > 861) throw std::length_error("turn_river_cards cannot have more than 861 elements");

			// we will mark with -1 
			std::fill(std::begin(cache), std::end(cache), -1);

			flop = _flop;
			turn_river_cards = _turn_river_cards;
			is_initialized = true;

			board[0] = flop[0];
			board[1] = flop[1];
			board[2] = flop[2];
	}


	int Cache::compute_hand_value(size_t c1_idx, size_t c2_idx, size_t turn_river_idx)
	{
		#if EXTRA_DEBUG_CHECKS
			if (c1_idx >= 52) throw std::out_of_range("bad card idx");
			if (c2_idx >= 52) throw std::out_of_range("bad card idx");
			if (turn_river_idx >= turn_river_cards.size()) throw std::out_of_range("bad turn_river idx");
			if (!is_initialized) throw std::runtime_error("cache not initialized");
		#endif

		// make sure c1_idx < c2_idx
		if (c1_idx > c2_idx) std::swap(c1_idx, c2_idx);
			
		// cache key: 6 bits c1_idx | 6 bits c2_idx | turn_river_idx (max 11 bits)
		std::uint32_t key = static_cast<std::uint32_t>((turn_river_idx*52*52) + (c2_idx*52) + c1_idx);
		int& eval = cache[key];
		if (eval < 0){
			auto& tr = turn_river_cards[turn_river_idx];
			board[3] = tr.first;
			board[4] = tr.second;
			board[5] = Card((unsigned char)c1_idx);
			board[6] = Card((unsigned char)c2_idx);

			eval = Eval::HandValue(board, 7);
			#if EXTRA_DEBUG_CHECKS
				if (eval < 0) throw std::runtime_error("got hand eval negative, should not happen");
			#endif
		}
		return eval;
	}


	int Cache::get_hand_value(size_t c1_idx, size_t c2_idx, size_t turn_river_idx) const
	{
		#if EXTRA_DEBUG_CHECKS
			if (c1_idx >= 52) throw std::out_of_range("bad card idx");
			if (c2_idx >= 52) throw std::out_of_range("bad card idx");
			if (turn_river_idx >= turn_river_cards.size()) throw std::out_of_range("bad turn_river idx");
			if (!is_initialized) throw std::runtime_error("cache not initialized");
		#endif

		// make sure c1_idx < c2_idx
		if (c1_idx > c2_idx) std::swap(c1_idx, c2_idx);
			
		// cache key: 6 bits c1_idx | 6 bits c2_idx | turn_river_idx (max 11 bits)
		std::uint32_t key = static_cast<std::uint32_t>((turn_river_idx*52*52) + (c2_idx*52) + c1_idx);
		int eval = cache[key];
		#if EXTRA_DEBUG_CHECKS
			if (eval < 0) throw std::runtime_error("got hand eval negative, should not happen");
		#endif
		return eval;
	}


	Cache& Cache::operator+=(const Cache& rhs)
	{
		if (!is_initialized) throw std::runtime_error("cache not initialized");		
		if (!rhs.initialized()) throw std::runtime_error("cache not initialized");		

		for (size_t i = 0; i < 52*52*861; i++)
			if (rhs.cache[i] >= 0)
				cache[i] = rhs.cache[i];

		return *this;
	}


}