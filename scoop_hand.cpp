#include "scoop_hand.h"
#include "eval.h"
#include "cache.h"

#include <set>
#include <numeric>
#include <random>
#include <algorithm>

namespace Scoop 
{
	using CardIdxSet = std::set<int>;

	Hand::Hand(std::span<Card> src) : score(0), n_score(0) { 
		if (src.size() != 7) throw std::length_error("wrong ScoopHand init number of hands, must be 7");
		for (size_t i = 0; auto& x:src)
			cards[i++] = x;
	}

	std::string Hand::to_str(){
		std::string s;
		s = cards[0].to_str() + cards[1].to_str() + "|";
		s += cards[2].to_str() + cards[3].to_str() + "|";
		s += cards[4].to_str() + cards[5].to_str();
		s += " (" + cards[6].to_str() + ")";
		return s;
	}





	double score_hero_vs_opp(std::span<Card> hero, std::span<Card> opp, std::span<Card> flop, std::span<Cards::Pair> turn_river_cards)
	{
		size_t i;
		float average_score = 0.0f;

		if (hero.size() != 7) throw std::length_error("hero must have 7 cards");
		if (opp.size() != 7) throw std::length_error("opp must have 7 cards");
		if (flop.size() != 3) throw std::length_error("flop must have 3 cards");
		if (turn_river_cards.size() < 1) throw std::length_error("turn_river_cards must have at least size 1");

		// top row gets 5x score multiplier, middle row 3x
		const int bonus_multiplier[3] = {5,3,1};

		// construct board in order to evaluate hands
		// board[0..2] flop cards
		// board[3..4] turn/river
		// board[5..6] hero/opp top/middle/bottom 2-cards
		Card board[7];
		for (i=0; i < 3; i++) board[i] = flop[i];

		for (auto& [turn,river]: turn_river_cards){
			board[3] = turn;
			board[4] = river;

			// score is computed from hero's point of view
			int score = 0;
			int n_scoop = 0;

			for (i=0; i<3; i++){
				board[5] = hero[i*2];
				board[6] = hero[i*2+1];
				int eval_hero = Eval::HandValue(board, 7);

				board[5] = opp[i*2];
				board[6] = opp[i*2+1];
				int eval_opp = Eval::HandValue(board, 7);

				if (eval_hero > eval_opp){
					n_scoop++;
					score += Eval::ScoopScore(eval_hero);
				} else if (eval_hero < eval_opp){
					n_scoop--;
					score -= Eval::ScoopScore(eval_opp);
				}
			}

			// if hero is better on all 3 rows: gets 30 points
			// same for opp
			if (n_scoop==3) 
				score += 30;
			else if (n_scoop==-3)
				score -= 30;

			average_score += score;
		}

		average_score /= turn_river_cards.size();

		return average_score;
	}


	double score_hero_vs_opp_w_cache(std::span<Card> hero, std::span<Card> opp, std::span<Card> flop, std::span<Cards::Pair> turn_river_cards, std::array<char,1024> &invalid_turn_river_idx_p1, std::array<char,1024> &invalid_turn_river_idx_p2)
	{
		size_t i, c1_idx, c2_idx;
		float average_score = 0.0f;

		if (hero.size() != 7) throw std::length_error("hero must have 7 cards");
		if (opp.size() != 7) throw std::length_error("opp must have 7 cards");
		if (flop.size() != 3) throw std::length_error("flop must have 3 cards");
		if (turn_river_cards.size() < 1) throw std::length_error("turn_river_cards must have at least size 1");

		// top row gets 5x score multiplier, middle row 3x
		const int bonus_multiplier[3] = {5,3,1};

		// construct board in order to evaluate hands
		// board[0..2] flop cards
		// board[3..4] turn/river
		// board[5..6] hero/opp top/middle/bottom 2-cards
		Card board[7];
		for (i=0; i < 3; i++) board[i] = flop[i];

		size_t n_boards = 0;
		for (size_t turn_river_idx=0; turn_river_idx < turn_river_cards.size(); turn_river_idx++){

			if (invalid_turn_river_idx_p1[turn_river_idx]) continue;
			if (invalid_turn_river_idx_p2[turn_river_idx]) continue;
			n_boards += 1;

			int score = 0;
			int n_scoop = 0;

			for (i = 0; i < 3; i++){
				c1_idx = hero[i*2].idx();
				c2_idx = hero[i*2+1].idx();
				int eval_hero = master_cache.get_hand_value(c1_idx, c2_idx, turn_river_idx);

				c1_idx = opp[i*2].idx();
				c2_idx = opp[i*2+1].idx();
				int eval_opp = master_cache.get_hand_value(c1_idx, c2_idx, turn_river_idx);

				if (eval_hero > eval_opp){
					n_scoop++;
					score += Eval::ScoopScore(eval_hero) * bonus_multiplier[i];
				} else if (eval_hero < eval_opp){
					n_scoop--;
					score -= Eval::ScoopScore(eval_opp) * bonus_multiplier[i];
				}
			}

			// if hero is better on all 3 rows: gets 30 points
			// same for opp
			if (n_scoop==3) 
				score += 30;
			else if (n_scoop==-3)
				score -= 30;

			average_score += score;
		}

		if (n_boards==0) throw std::runtime_error("0 turn rivers, should not happen");

		average_score /= n_boards;

		return average_score;
	}


	// when we generate permutations we will store hands iff top/middle/bottom 2 cards indices are in ascending order
	// this way we avoid a lot of duplicates
	static bool filter_scoop_hand(std::span<Card> hand)
	{
		if (hand[0].idx() <= hand[1].idx()) return false;
		if (hand[2].idx() <= hand[3].idx()) return false;
		if (hand[4].idx() <= hand[5].idx()) return false;
		return true;
	}

	auto card_cmp = [](const Card& c1, const Card& c2) { return c1.idx() < c2.idx(); };

	std::vector<Hand> gen_all_possible_hands(std::array<Card,7> cards)
	{
		std::vector<Hand> all_possible_hero_hands;
		std::sort(cards.begin(), cards.end(), card_cmp);
		do {
			if (filter_scoop_hand(cards))
				all_possible_hero_hands.push_back(Hand(cards));
		} while (std::next_permutation(cards.begin(), cards.end(), card_cmp));

		//std::shuffle(all_possible_hero_hands.begin(), all_possible_hero_hands.end(), std::random_device());

		return all_possible_hero_hands;
	}


	// helper function
	static void remove_from_card_idx_set(auto& set, std::span<Card> cards)
	{
		for (auto& c: cards)
			set.erase(c.idx());
	}

	std::vector<Cards::Pair> gen_all_turn_river_cards(std::span<Card> cards, std::array<Card,3> flop)
	{
		CardIdxSet all_cards;
		for (int i = 0; i < 52; i++) all_cards.insert(i);

		CardIdxSet cards_left = all_cards;
		remove_from_card_idx_set(cards_left, cards);
		remove_from_card_idx_set(cards_left, flop);

		std::vector<Cards::Pair> turn_river;
		for (int i: cards_left)
			for (int j: cards_left)
				if (i < j)
					turn_river.push_back(std::make_pair(Card(i), Card(j)));
		return turn_river;
	}

	// returns a list of indices in turn_river_cards that contain any card from cards_to_exclude
	std::array<char,1024> gen_all_invalid_turn_rivers_indices(std::span<Cards::Pair> turn_river_cards, std::span<Card> cards_to_exclude)
	{
		std::array<char,52> exclude{};
		for (auto& card : cards_to_exclude)
			exclude[card.idx()] = 1;
				
		std::array<char,1024> ret{0};
		for (size_t i = 0; auto& [turn,river]: turn_river_cards){
			if (exclude[turn.idx()] || exclude[river.idx()])
				ret[i] = 0;
			i++;
		}

		return ret;
	}


}

