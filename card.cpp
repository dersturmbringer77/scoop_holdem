#include "card.h"

// init a card by index 0..53
Card::Card(unsigned char idx){
	if (idx > 53) throw std::out_of_range("card index can only be 0..53");
	m_idx = idx;
}

Card::Card(const std::string& s){
	if (s.size() < 2) throw std::length_error("card init from string needs 2 characters");

	static const char value[14] = {'2','3','4','5','6','7','8','9','T','J','Q','K','A','*'};
	static const char suite[14] = {'h','d','c','s'};

	size_t i;
	bool found;
	for (i = 0, found=false; i < 14; i++) 
		if (s[0] == value[i]) {
			m_idx = (char)(i<<2);
			found = true;
		}
	if (!found) throw std::runtime_error("invalid card value");
	for (i = 0, found=false; i < 4; i++) 
		if (s[1] == suite[i]) {
			m_idx |= (char)i;
			found = true;
		}
	if (!found) throw std::runtime_error("invalid card suite");
}

// init a card by value 2..15 (JOKER) and suite 0..3 (Heart/Diamond/Club/Spade)
Card::Card(unsigned char value, unsigned char suite) {
	if (value < 2 || value > 15) throw std::out_of_range("card value can only be 2..15");
	if (suite < 0 || suite >= 4) throw std::out_of_range("card suite can only be 0..3");
	m_idx = (value-2)*4+suite;
}

std::string Card::to_str() {
	static const char value[14] = {'2','3','4','5','6','7','8','9','T','J','Q','K','A','*'};
	static const char suite[14] = {'h','d','c','s'};
	std::string s;
	s = value[this->value()-2];
	s += suite[this->suite()];
	return s;
}
