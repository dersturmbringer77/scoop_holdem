# Solving Scoop Holdem using C++

## Description

Scoop Hold'em is a card game available from Upoker <http://upoker.net> 

Here are full rules as taken from UPoker app: <br>

![Rules1](</rules1.png>) ![Rules2](</rules2.png>) ![Rules3](</rules3.png>)

## Working out the code

We want to write a solver that - given hero's 7 hole cards and the 3 flop cards as command line arguments - will compute a good solution vs one or two opponents - and will compute it fast (nobody likes waiting too much!). <br> 

First we need a way to represent a playing card: <br> 

```c++
class Card {
public:
	// by default a card will be 2H
	Card() : m_idx(0) {}
		
	// init a card by index 0..53
	Card(unsigned char idx){
		if (idx > 53) throw std::out_of_range("card index can only be 0..53");
		m_idx = idx;
	}

	// inits card from the first 2 characters in a string
	Card(const std::string& s);

	// init a card by value 2..15 (JOKER) and suite 0..3 (Heart/Diamond/Club/Spade)
	Card(unsigned char value, unsigned char suite);

	unsigned char idx() const { return m_idx; }
	unsigned char value() const { return (m_idx>>2)+2; }
	unsigned char suite() const { return m_idx&3; }

	// returns a string representation of the card
	std::string to_str();

protected:
	unsigned char m_idx;	
};
```

Then we need a way to represent a Scoop hand, i.e. the 7 cards a player is being dealt. <br> 
We create our own struct Scoop::Hand that will include a 7 Card array and two internal variables that will be used when computing each hand score and sorting hands descending based on score. <br> 
Since card order matter we agree by convention to store top 2 row as first 2 cards, middle row as next 2 cards, bottom row as next 2 cards and discarded card as last one. <br> 
We could have created top/middle/bottom/discard members but for this example I think this would have complicated the code a lot. <br> 

In addition to Hand struct we create also a few helpful functions to manipulate Hands, all in the Scoop namespace:<br> 
* score_hero_vs_opp_w_cache: given a hero hand and an opponent hand we compute average score of hero vs opp over all valid turn+river cards (those that do not have cards in common with either player, since that would be impossible)<br> 
* gen_all_possible_hands: for a 7 card array generates all possible hands (with the convention above)<br> 
* gen_all_turn_river_cards: given a set of known cards will generate all unique turn+river pairs of possible hands<br> 

```c++
namespace Scoop {

	struct Hand {	
		Hand(){}
		Hand(std::span<Card> src);

		std::string to_str();

		std::array<Card,7> cards;
		double score;	// useful when compiling hand score and sorting by it
		int n_score;
	};

	float score_hero_vs_opp_w_cache(std::span<Card> hero, std::span<Card> opp, std::span<Card> flop, std::span<Cards::Pair> turn_river_cards, std::array<char,1024> &invalid_turn_river_idx_p1, std::array<char,1024> &invalid_turn_river_idx_p2);

	std::vector<Hand> gen_all_possible_hands(std::array<Card,7> cards);

	// given 7 cards for hero and 3 cards for flop: generates a vector of all distinct card pairs for possible turn+rivers
	// turn_card.idx() < river_card.idx() always
	std::vector<Cards::Pair> gen_all_turn_river_cards(std::span<Card> cards, std::array<Card,3> flop);

	// returns a list of indices in turn_river_cards that contain any card from cards_to_exclude
	std::array<char,1024> gen_all_invalid_turn_rivers_indices(std::span<Cards::Pair> turn_river_cards, std::span<Card> cards_to_exclude);
}
```

For evaluating 7 cards poker hands we will use PokerEval library (https://github.com/v2k/poker-eval)<br>
It is a handy and fast library that will generate an integer value for each given poker hand, the bigger this number the stronger the poker hand <br>
Here is the wrapper code I wrote for evaluating Scoop hands and quickly computing score from the int value:<br>

```c++
namespace Eval {

	// function used internally by Eval_HandValue 
	// we could later replace it with a lookup table for speed since we have only 54 possible cards
	static uint64 ConvertToCardMask(Card& c0){
		CardMask c;
		c.cards_n = 0;
		switch(c0.suite()){
		case 0: c.cards.hearts = (1 << (c0.value()-2)); break;
		case 1: c.cards.diamonds = (1 << (c0.value()-2)); break;
		case 2: c.cards.clubs = (1 << (c0.value()-2)); break;
		case 3: c.cards.spades = (1 << (c0.value()-2)); break;
		}
		return c.cards_n;
	}


	// returns poker_eval score for a given poker hand
	int HandValue(Card* cards, int nrCards)
	{
		int i;
		CardMask c;
		c.cards_n = 0;
		for (i = 0; i < nrCards; i++) c.cards_n |= ConvertToCardMask(cards[i]);
		return Hand_EVAL_N(c, nrCards);
	}


	// quick and dirty function that returns SCOOP score for a 7 cards hand score (returned by Eval_HandValue)
	int ScoopScore(int v)
	{
		#define _check(X,VAL) if ((v & X)==X) return VAL;

		v >>= 24;
		_check(StdRules_HandType_STFLUSH, 20);
		_check(StdRules_HandType_QUADS, 10);
		_check(StdRules_HandType_FULLHOUSE, 6);
		_check(StdRules_HandType_FLUSH, 5);
		_check(StdRules_HandType_STRAIGHT, 4);
		_check(StdRules_HandType_TRIPS, 3);
		_check(StdRules_HandType_TWOPAIR, 2);
		return 1;
	}

}
```

As mentioned we could make it even faster with lookup tables but for this solver we get fast enough for our purposes, so no need for that. <br> 
<br>
So far so good, but we don't want to recompute hand value for each 2 card pair hero has on each row, especially since we will have the same cards on different rows. <br>
Hence we need a way to cache hand values. Enter the Scoop::Cache struct: <br> 

```c++
namespace Scoop {

	struct Cache {
		Cache();
		
		void init(std::span<Card> _flop, std::span<Cards::Pair> _turn_river_cards);
		int compute_hand_value(size_t c1_idx, size_t c2_idx, size_t turn_river_idx);
		int get_hand_value(size_t c1_idx, size_t c2_idx, size_t turn_river_idx) const;
		Cache& operator+=(const Cache& rhs);

		bool initialized() const;
		
	protected:
		bool is_initialized;
		Card board[7];
		std::span<Card> flop;
		std::span<Cards::Pair> turn_river_cards;
		std::int32_t cache[52*52*861];
	};

	extern Cache master_cache;

}
```

Without going too much into details we compute exactly once for each pair of c1/c2 cards and each existing turn+river pair the value of the holdem hand as returned by poker-eval library. <br>
We don't compute for all card pairs, we will generate a number of random opponents and compute only values of card pairs existing in opponents hands. <br>
We use 
```c++
		std::int32_t cache[52*52*861];
```
to store cache values, initialized with -1 for inexistent values (since poker-eval will return values >= 0 for poker hands) <br> 
Not all entries will be used, since when we compute values for 2 cards c1 and c2 we make sure that c1.idx() < c2.idx() (else we swap them), however we can access very fast elements in this sparsely populated cache table. <br> 
861 is the number of possible turn+river unique pairs, since we have 10 known cards out of a 52 cards deck we are left with 42 unknown turn+river cards, so the number of unique pairs is 
```
42*41/2 = 861
```

Having a cache is a good first step in order to speed things up, however what if we want to compute results in parallel on computers with multiple cores ? <br> 
Having the cache defined as above is very helpful, we can safely assign a list of players to compute cache for to each thread + a Scoop::Cache structure, have them evaluate hands in parallel then concatenate all the results in a master cache. <br>
Basically something like this: <br> 
```c++

#include <thread>

struct CachePayload {
	std::vector<PlayerInfo> players;
	std::vector<Scoop::Cache> cache;
	std::span<Cards::Pair> turn_river;
};

void fill_cache_func(CachePayload& payload)
{
	size_t i,j,turn_river_idx;
	for (i = 0; i < payload.players.size(); i++){
		auto& p = payload.players[i];
		auto& cache = payload.cache[i];
		for (auto& h: p.all_possible_hands){
			for (j = 0; j < 3; j++){
				size_t c1_idx = h.cards[j*2+0].idx();
				size_t c2_idx = h.cards[j*2+1].idx();
				for (turn_river_idx = 0; turn_river_idx < payload.turn_river.size(); turn_river_idx++)
					cache.compute_hand_value(c1_idx, c2_idx, turn_river_idx);
			}
		}
	}
}


// for opponents and hero fills a cache with all possible hand values for all values for turn/river
// p0 is our hero
// players is the list of opponents
// it will assign to each thread a number of players and evaluate all hands
// when all is done will concatenate all computed hand values to a master cache
void fill_master_cache(PlayerInfo& p0, std::vector<PlayerInfo>& players, HandInfo& hand_info)
{
	size_t i, current_thread;
	std::array<std::jthread, N_PROC> threads;
	std::array<CachePayload, N_PROC> payload;

	Scoop::master_cache.init(hand_info.flop, hand_info.turn_river);

	// prepare threads
	for (i = 0; i < N_PROC; i++)
		payload[i].turn_river = hand_info.turn_river;

	// assign players to threads
	current_thread=0;
	payload[current_thread].players.push_back(p0);
	payload[current_thread].cache.push_back(Scoop::master_cache);		
	current_thread = (current_thread+1)%N_PROC;
	for (i = 0; i < players.size(); i++, current_thread = (current_thread+1)%N_PROC){
		payload[current_thread].players.push_back(players[i]);
		payload[current_thread].cache.push_back(Scoop::master_cache);		
	}

	// runs them
	for (i = 0; i < N_PROC; i++)
		threads[i] = std::jthread([&](size_t id) { fill_cache_func(payload[id]); }, i);

	for (i = 0; i < N_PROC; i++)
		threads[i].join();

	// concatenate results, this is the precise reason we overloaded Cache::operator+=(...)
	for (i = 0; i < N_PROC; i++)
		for (auto& cache: payload[i].cache)
			Scoop::master_cache += cache;
}
```
Here I used one Scoop::Cache for each player, there is no reason why I should have not used one Scoop::Cache per thread instead - but since code is still very fast I will let it as it is for now <br>
<br>
Almost there, one final problem to solve. Suppose that we start with each opponent sorting his hands by absolute points, descending order. <br> 
That would be first pass. <br>
Then each opponents will sort his hands by strength vs top hands of other opponents <br>
That would be the second pass. <br>
Finally if the opponent is sneaky he will sort again his hands vs the top hands of other opponents from the previous pass <br>.
Then for the hero we just need to compute hand strength vs one and two simultaneous opponents top hands. <br> 
At first I tried some Counterfactual Regret Minimization algorithm (https://towardsdatascience.com/lairs-dice-by-self-play-3bbed6addde0) in order to get the right results, however the above two pass algorithm seems to produce best results and is simple to understand. <br> 
<br>
Solving one pass for the opponents with multithreaded code looks something like this: 
```c++
struct PlayerPassPayload {
	float threshold;
	bool just_top_solution;
	std::span<Card> flop;
	std::vector<PlayerInfo> players;
	std::span<Cards::Pair> turn_river;
};

void player_pass_func(PlayerPassPayload& payload)
{
	auto new_players(payload.players);
	for (auto& p: new_players) p.all_possible_hands = get_top_hands(p, payload.threshold);

	for (auto& p1: payload.players){
		for (auto& h1: p1.all_possible_hands){
			h1.score = 0;
			h1.n_score = 0;

			for (auto& p2: new_players){
				if (p2.card_mask & p1.card_mask) continue;
				double score = 0;
				int n = 0;
				for (auto& h2: p2.all_possible_hands){
					score += (double)Scoop::score_hero_vs_opp_w_cache(h1.cards, h2.cards, payload.flop, payload.turn_river, p1.invalid_turn_river_idx, p2.invalid_turn_river_idx);
					n += 1;
					if (payload.just_top_solution) break;
				}
				h1.score += score/n;
				h1.n_score += 1;
			}
			h1.score /= h1.n_score;
		}
	}
	for (auto& p1: payload.players)
		std::sort(p1.all_possible_hands.begin(), p1.all_possible_hands.end(), [](const ScoopHand& left, const ScoopHand& right) { return left.score > right.score; });
}



std::vector<PlayerInfo> do_pass_multithreaded(std::vector<PlayerInfo>& players, HandInfo& hand_info, float threshold)
{
	size_t i, current_thread;
	std::array<std::jthread, N_PROC> threads;
	std::array<PlayerPassPayload, N_PROC> payload;

	// prepare threads
	for (i = 0, current_thread=0; i < players.size(); i++, current_thread = (current_thread+1)%N_PROC)
		payload[current_thread].players.push_back(players[i]);
	for (i = 0; i < 5; i++){
		payload[i].flop = hand_info.flop;
		payload[i].threshold = threshold;
		payload[i].turn_river = hand_info.turn_river;
		payload[i].just_top_solution = threshold >= 0.99;
	}

	// runs them
	for (i = 0; i < N_PROC; i++)
		threads[i] = std::jthread([&](size_t id) { player_pass_func(payload[id]); }, i);

	for (i = 0; i < N_PROC; i++)
		threads[i].join();

	// return new player list
	std::vector<PlayerInfo> new_players;
	for (i = 0; i < N_PROC; i++)
		for (auto& p: payload[i].players)
			new_players.push_back(p);
	return new_players;
}
```

## Putting it all together

With the above written and explained, computing hero strategy versus two-pass opponents and three-pass opponents looks like this: <br>

```c++
void solve_hero_vs_many_opp(std::array<Card,7> hero_cards, std::array<Card,3> flop, const int N_OPP=50)
{
	size_t i;

	auto turn_river = Scoop::gen_all_turn_river_cards(hero_cards, flop);
	auto all_hero_possible_hero_hands = Scoop::gen_all_possible_hands(hero_cards);

	HandInfo hand_info(flop, turn_river);

	// init hero info
	PlayerInfo p0;
	p0.cards = hero_cards;
	p0.all_possible_hands = Scoop::gen_all_possible_hands(hero_cards);
	p0.card_mask = gen_card_mask(hero_cards);
	std::fill(p0.invalid_turn_river_idx.begin(), p0.invalid_turn_river_idx.end(), 0);
	p0.id = 0;

	// determine remaining cards in deck
	std::int64_t seen_cards=0;
	for (auto& c: flop) seen_cards |= (1ll << c.idx());
	for (auto& c: hero_cards) seen_cards |= (1ll << c.idx());

	std::vector<Card> cards_in_deck;
	for (i = 0; i < 52; i++)
		if (((seen_cards>>i)&1) == 0)
			cards_in_deck.push_back(Card((unsigned char)i));

	// generate random N_OPP opponents
	std::vector<PlayerInfo> players;
	auto gen = std::mt19937{std::random_device{}()};
	for (i = 0; i < N_OPP; i++){
		PlayerInfo p;

		std::vector<Card> cards_sample;
		std::sample(cards_in_deck.begin(), cards_in_deck.end(), std::back_inserter(cards_sample), 7, gen);
		std::copy(cards_sample.begin(), cards_sample.end(), p.cards.begin());

		p.all_possible_hands = Scoop::gen_all_possible_hands(p.cards);
		p.card_mask = gen_card_mask(p.cards);
		p.invalid_turn_river_idx = Scoop::gen_all_invalid_turn_rivers_indices(turn_river, p.cards);
		p.id = players.size();
		players.push_back(p);
	}
	
	fill_master_cache(p0, players, hand_info);

	// for each player keep top hands fir first pass...
	keep_top_hands(p0.all_possible_hands, turn_river);
	for (auto& p: players)
		keep_top_hands(p.all_possible_hands, turn_river);

	players = do_pass_multithreaded(players, hand_info, 1.0f);	// first pass vs naive hands
	players = do_pass_multithreaded(players, hand_info, 0.85f); // 2nd pass vs hands from 1st pass

	auto best_hand = compute_best_hand_vs_one_opp(p0, players, flop, turn_river, 0.99f);
	auto best_hand2 = compute_best_hand_vs_two_opp(p0, players, flop, turn_river, 0.99f);

	cout << "[pass2]\n\n";
	cout << "[vs one player]:  "<< best_hand.to_str() << " -> " << best_hand.score << endl;
	cout << "[vs two players]: "<< best_hand2.to_str() << " -> " << best_hand2.score << endl;

	cout << "\n[pass3]\n\n";

	players = do_pass_multithreaded(players, hand_info, 0.85f); // 3rd pass: vs 2nd pass hands

	best_hand = compute_best_hand_vs_one_opp(p0, players, flop, turn_river, 0.99f);
	best_hand2 = compute_best_hand_vs_two_opp(p0, players, flop, turn_river, 0.99f);

	cout << "[vs one player]:  "<< best_hand.to_str() << " -> " << best_hand.score << endl;
	cout << "[vs two players]: "<< best_hand2.to_str() << " -> " << best_hand2.score << endl;

	cout << endl;
}
```

## Benchmarking

When it comes to game solvers in my experience never ever trust your gut instinct and always test solvers versus a benchmark of games. <br> 
Which is exactly what I did, even if I didn't include (yet) the benchmark code in the repository, perhaps I will do that later after I clean up the code <br> 
I benchmarked the solver versus a previous solver version on a 4000 hands dataset. <br> 
It managed to beat the old version for over 0.75 points per hand. <br> 
Pretty good but for better results I would need a few other benchmarks: one with hands vs one opponent playing the 2 pass strategy, one vs one opponent playing the 3 pass strategy and another two vs two opponents playing 2/3 pass strategy each. Then play with solver parameters until it will be able to increase the score in those benchmarks.

## Technical side
Project is written in Visual Studio 2019 but there should be no reason for it not to compile with any C++20 compatible compiler.<br> 
With some small modifications should work on C++17 compilers (replace std::span with something else and use std::thread instead of std::jthread).<br> 

## Final words

Nothing earth shattering, just an exercise in optimization at algorithmic level. <br> 
However I do like how it turned out, I believe the code is pretty clear to understand and compact. <br>
I managed to to cut solving time from over 1 minute per hand to under 2 seconds on a 4 core i7 using 5 threads. <br>
<br>
Never ever use the solver online, moral issues aside each time there are 2 or 3 opponents they might be colluding and sharing cards and that changes computations completely. <br>
<br>
Perhaps in the future I will try Counterfactual Regret computations again, see if I can get better results. Or use neural networks and reinforcement learning. Both approaches might be interesting to try, although so far experience tells me that when precise search is possible it produces better results.

